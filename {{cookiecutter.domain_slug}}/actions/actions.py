from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

# https://rasa.com/docs/rasa/custom-actions
# https://rasa.com/docs/action-server/sdk-actions


#
# This file is part of a loose module called actions.
#
# Once this domain installed, this module will take the name of the domain.
#
# So don't use `from actions.my_file import stuff` but rather `from .my_file import stuff`.
#

# class ActionRecipe(Action):

#     def name(self) -> Text:
#         intent_name = "an_intent_here"
#         return f"action_{intent_name}"

#     async def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
#         dispatcher.utter_message(text="This is a message")
#         matches = resp.json()['data']['docs'][0]['matches']
        
#         return [] # Return the next state (or listen)
