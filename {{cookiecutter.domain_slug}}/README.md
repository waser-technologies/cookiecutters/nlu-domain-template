# {{cookiecutter.domain_name}}: NLU Domain

{{cookiecutter.domain_description}}

## Installation

Use the [Domain Management Tool](https://gitlab.com/waser-technologies/technologies/dmt) to effortlessly install, validate and train a new NLP model using this domain.

```zsh
dmt -V -T -a https://github.com/{{cookiecutter.github_username}}/{{cookiecutter.domain_slug}}.git
```

## Usage

To test the model on this domain, use the `dmt` to serve the latest model.

```zsh
dmt -S
```
This will take a while to load.

Once you see:
> `INFO     root  - Rasa server is up and running.`

You can query the server in another terminal.

```zsh
curl -XPOST localhost:5005/webhooks/rest/webhook -s -d '{ "message": "Hello", "sender": "user" }'
```

Or simply use [`Assistant`](https://gitlab.com/waser-technologies/technologies/assistant).

```zsh
assistant Hello
```

## Contributions

This domain was jumpstarted using the [NLP Domain Template](https://gitlab.com/waser-technologies/cookiecutters/nlu-domain-template).