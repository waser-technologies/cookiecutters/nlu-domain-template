# Template for NLP Domains

This CookieCutter template is here to help you jumpstart the creation of your next NLP domain.

## Usage

Create a new NLP domain using the [Domain Management Tool](https://gitlab.com/waser-technologies/technologies/dmt).

```zsh
dmt --create
```

Or with `cookiecutter` directly.

```zsh
pip install -U cookiecutter
cookiecutter https://gitlab.com/waser-technologies/cookiecutters/nlu-domain-template
```

### Validate your domain

```zsh
make validate
```

### Train models with your domain

```zsh
make train
```

### Test your models

Once you have successfuly trained a model.

```zsh
make shell
```

Note: you need to run actions separately.

For example with:

```zsh
make serve
```

Or using the [Domain Management Tool](https://pypi.org/project/dmt/) `--serve` flag.

## Data structure

All data related to your domain sould be stored in `data/`.

### Intent Classification & Entity Recognition

Under `data/nlu/`, you'll find everything related to intent classes and entities extraction.

Take a look at [SmallTalk](https://gitlab.com/waser-technologies/data/nlu/en/smalltalk) as a reference to build your own domains.

```zsh
.
├── data/
│   ├── nlu/
│   │   ├── *.yml
│   │   └── verbs.yml
```

> `verbs.yml`
> is the principal file.
> It contains *intents* for *verbs*.

> `*.yml`
> -> When a *verb* is complex enough it is moved to its own file.

> `/!\` For each *verb* with its own file, you should create a [ResponseSelector]() for the *verb*.

#### *Intent*:

A *verb*, its *target*, its *subject*, its *target' subject*.

> But it's pronounced *target' subject* *target* *subject* *verb*.

```yml
#!data/nlu/can.yml

version: "{{cookiecutter.rasa_version}}"
nlu:
# talk you cant
- intent: can_not/talk_you
  examples: |
    - You can't talk
    - You don't have the right to talk
    - Don't talk
    - silent
    - silence
```

```yml
#!data/nlu/imagine.yml

version: "{{cookiecutter.rasa_version}}"
nlu:
# intent: <VERB>(/<VERB_TARGET>)(_<VERB_SUBJECT>)(_<VERB_TARGET_SUBJECT>)
- intent: imagine/look_you_me
# To better understand an intent, try say'ng it like this:
# <VERB_TARGET_SUBJECT> <VERB_TARGET> <VERB_SUBJECT> <VERB>
# me look you imagine
# Or if you prefer: My look, you imagine.
  examples: |  
    - what do you think I look like
    - imagine how I look
```

#### *Entity*:

One good *slot* filler tool.

Enities are wraped in brackets and followed by their type in parenthesys.

```yml
#!data/nlu/speak.yml

version: "{{cookiecutter.rasa_version}}"
nlu:
# languages which you speak (remember, target' subject, target, verb' subject and verb)
- intent: speak/which_you_languages
  examples: |
    - Do you speak [german](language)?
    - Do you understand [spanish](language)?
```

```yml
#!data/nlu/verbs.yml

version: "{{cookiecutter.rasa_version}}"
nlu:
# This is a special intent to input data like your name, an amount of money, dates, etc.
- intent: enter_data
  examples: |
    - I am [Christina Sullivan](name)
    - I work as a [frontend dev](job_function)
    - I work for the [New York Times](organisation)
    - get [dates](entity) from messages
    - how much [money](entity)

```

#### *Response*:

Utterance that answers an *intent*.

```zsh
.
├── data/
│   ├── nlu/
│   │   ├── responses/
│   │   │   ├── *.yml
│   │   │   └──responses.yml
```

> `responses.yml`
> is the principal file.
> It contains *responses* to *intents*.

> `*.yml`
> -> There is a *response* file for every *verb* that has its own file.

> `(i)`
> The *response* file is named after the *verb* it responds to.
> (e.g. : `data/nlu/be.yml` -> `data/nlu/responses/be.yml`)

A *response* is a basic action that return static text based on the intent.

It always starts with `utter_` followed by the name of the *intent* it responds to.

Therefor `utter` becomes `respond`.

Which gives: `respond {intent}`.

Here is a concrete example extracted from `apologize`.

```yml
#!data/nlu/apologize.yml


version: "{{cookiecutter.rasa_version}}"
nlu:
# Intent: you apologize
- intent: apologize/me_you
  examples: |
    - excuse you
    - watch yourself
    - apologise
    - apologize now
    - redeem yourself
    - apologies to me
# Intent: me apologize
- intent: apologize/you_me
  examples: |
    - excuse me
    - I apologize
    - sorry
    - I'm sorry
```


```yml
#!data/nlu/responses/apologize.yml

version: "{{cookiecutter.rasa_version}}"
responses:
# Action: respond apologize
# Here the intent only contains a *verb*.
# It should be understood as an injuction.
# e.g.: 'Apologise, now!'
  utter_apologize:
  - text: My apologies.
  - text: I'm deeply sorry.
  - text: I am very sorry.
  - text: My excuses
  - text: I would like you to pardon me
  - text: Pardon me.
  - text: You should know that I'm sorry.

# Action: respond me you apologize
# Here *me* wants *apologize* from *you*.
# Which it's basically the same as saying *apologize*.
# We won't repeat ourselfs,
# we'll use the default response of this verb instead.
# e.g.: 'Excuse you!'
  utter_apologize/me_you:
  - template: utter_apologize

# Action: respond you me apologize
# Here the intent describes *me* *apologize* to *you*.
# It should be understood as an excuse from *me*.
# e.g.: 'Sorry'
  utter_apologize/you_me:
  - text: I pardon you.
  - text: You are excused.
  - text: Well, please be mindful.
  - text: I do.
  - text: Let's not talk about it any further.
  - text: I don't need to ear it.
  - text: No need, really.
  - text: You don't need to say it.
  - text: Forget about it!
```

### *Stories*:

Readme a *story*

```zsh
.
├── data/
│   ├── stories/
│   │   ├── *.yml
│   │   └── stories.yml
```

```yml
#!data/stories/stories.yml

version: "{{cookiecutter.rasa_version}}"
stories:
- story: user is glad to meet assistant
  steps:
  - intent: greet # Hi
  - action: utter_greet # Hey
  - intent: glad # Nice to meet you
  - action: utter_glad # It my pleasure really
```

## Further documentation

A domain is composed of multiple components from the [RASA Open Source](https://github.com/RasaHQ/rasa) project.

- [Configuration](https://rasa.com/docs/rasa/model-configuration) (`config.yml`)
- [Domain Card](https://rasa.com/docs/rasa/domain) (`domain.yml`)
- [Connectors Credentials](https://rasa.com/docs/rasa/messaging-and-voice-channels/) (`credentials.yml`)
- [Domain Testing](https://rasa.com/docs/rasa/testing-your-assistant) (`tests/*.yml`)

You can also create custom actions for the [RASA Action Server](https://rasa.com/docs/action-server/).

- [Actions](https://rasa.com/docs/action-server/sdk-actions) (`actions/*.py`)
